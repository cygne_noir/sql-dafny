include "SQLTypeSystem.dfy"
include "SQLSemantics.dfy"
include "SoundnessAuxDefs.dfy"
include "ProgressProof.dfy"

module Preservation
{
  import opened Environments
  import opened Tables
  import opened TableTypes
  import opened Opt
  import opened Queries
  import opened TypeSystem
	import opened QuerySemantics
	import opened StoreContextConsistency
  import opened Progress

  lemma rawUnionPreservesWellTypedRaw<N, FType, Val>(rt1: RawTable<Val>, rt2: RawTable<Val>, tt: TType<N, FType>)
    requires welltypedRawtable(tt, rt1)
    requires welltypedRawtable(tt, rt2)
    ensures welltypedRawtable(tt, rawUnion(rt1, rt2))
  {}

  lemma rawIntersectionPreservesWellTypedRaw<N, FType, Val>(rt1: RawTable<Val>, rt2: RawTable<Val>, tt: TType<N, FType>)
    requires welltypedRawtable(tt, rt1)
    requires welltypedRawtable(tt, rt2)
    ensures welltypedRawtable(tt, rawIntersection(rt1, rt2))
  {}

  lemma rawDifferencePreservesWellTypedRaw<N, FType, Val>(rt1: RawTable<Val>, rt2: RawTable<Val>, tt: TType<N, FType>)
    requires welltypedRawtable(tt, rt1)
    requires welltypedRawtable(tt, rt2)
    ensures welltypedRawtable(tt, rawDifference(rt1, rt2))
  {}


  lemma projectTypeAttrLMatchesAttrL<N, FType>(al: AttrL<N>, tt: TType<N, FType>)
		requires projectTypeAttrL(al, tt).some?
		ensures matchingAttrL(projectTypeAttrL(al, tt).get, al)
	{}

	lemma welltypedEmptyProjection<N, FType, Val>(rt: RawTable<Val>)
		ensures welltypedRawtable<N, FType, Val>([], projectEmptyCol<Val>(rt))
	{}

	lemma projectFirstRawPreservesWelltypedRaw<N, FType, Val>(a: N, ct: FType, t: Table<N, Val>, tt: TType<N, FType>)
		requires |tt| >= 1
		requires welltypedRawtable(tt, t.getRaw)
		requires tt[0] == (a, ct)
		ensures welltypedRawtable([(a, ct)], projectFirstRaw(t.getRaw))
		decreases |t.getRaw|
	{
		if |t.getRaw| == 0
		{}
		else
		{
			if (t.getRaw)[0] == []
			{}
			else
			{
				projectFirstRawPreservesWelltypedRaw(a, ct, table(t.getAL, (t.getRaw)[1..]), tt);
			}
		}
	}

	lemma findColPreservesWelltypedRaw<N, FType, Val>(a: N, al: AttrL<N>, rt: RawTable<Val>, tt: TType<N, FType>)
		requires welltypedRawtable(tt, rt)
		requires matchingAttrL(tt, al)
		requires findColType(a, tt).some?
		requires findCol(a, al, rt).some?
		ensures welltypedRawtable([(a,findColType(a, tt).get)], findCol(a, al, rt).get)
		decreases |al|
	{
		if |al| == 0
		{} //case not possible
		else
		{
			if a == al[0]
			{
				projectFirstRawPreservesWelltypedRaw(a, findColType(a, tt).get, table(al, rt), tt);
			}
			else
			{
				dropFirstColRawPreservesWelltypedRaw(tt, rt);
				findColPreservesWelltypedRaw(a, al[1..], dropFirstColRaw(rt), tt[1..]);
			}
		}
	}

	lemma attachColToFrontRawPreservesWelltypedRaw<N, FType, Val>(tt1: TType<N, FType>, tt2: TType<N, FType>, rt1: RawTable<Val>, rt2: RawTable<Val>)
		requires |tt1| == 1
		requires |rt1| == |rt2| 
		requires welltypedRawtable(tt1, rt1)
		requires welltypedRawtable(tt2, rt2)
		ensures welltypedRawtable(tt1 + tt2, attachColToFrontRaw(rt1, rt2))
	{}

	lemma attachColToFrontRawPreservesRowCount<N, FType, Val>(tt1: TType<N, FType>, rt1: RawTable<Val>, rt2: RawTable<Val>)
		requires |tt1| == 1
		requires welltypedRawtable(tt1, rt1)
		requires |rt1| == |rt2|
		ensures |rt1| == |attachColToFrontRaw(rt1, rt2)|
	{}

	lemma projectFirstRawPreservesRowCount<Val>(rt: RawTable<Val>)
		ensures |rt| == |projectFirstRaw(rt)|
	{}

	lemma dropFirstColRawPreservesRowCount<Val>(rt: RawTable<Val>)
		ensures |rt| == |dropFirstColRaw(rt)|
	{}

	lemma findColPreservesRowCount<N, Val>(a: N, al: AttrL<N>, rt: RawTable<Val>)
		requires findCol(a, al, rt).some?
		ensures |rt| == |findCol(a, al, rt).get|
	{
		if |al| == 0
		{}
		else
		{
			if a == al[0]
			{projectFirstRawPreservesRowCount(rt);}
			else
			{dropFirstColRawPreservesRowCount(rt);}
		}
	}


	lemma projectEmptyColPreservesRowCount<Val>(rt: RawTable<Val>)
		ensures |rt| == |projectEmptyCol(rt)|
	{}

	lemma projectColsPreservesRowCount<N, FType, Val>(tt: TType<N, FType>, al1: AttrL<N>, al2: AttrL<N>, rt: RawTable<Val>)
		requires projectTypeAttrL(al1, tt).some?
		requires projectCols(al1, al2, rt).some?
		requires welltypedRawtable(tt, rt)
		requires matchingAttrL(tt, al2)
		ensures |rt| == |projectCols(al1, al2, rt).get|
	{
		if |al1| == 0
		{projectEmptyColPreservesRowCount(rt);}
		else
		{
			if findCol(al1[0], al2, rt).some?
			{
				findColPreservesRowCount(al1[0], al2, rt);				
				projectColsPreservesRowCount(tt, al1[1..], al2, rt);
				assert findColType(al1[0],tt).some?;
				findColPreservesWelltypedRaw(al1[0], al2, rt, tt);
				attachColToFrontRawPreservesRowCount([(al1[0], findColType(al1[0], tt).get)],
					findCol(al1[0], al2, rt).get, projectCols(al1[1..], al2, rt).get);
			}
			else
			{}
		}
	}
	
	lemma projectColsWelltypedWithSelectType<N, FType, Val>(al: AttrL<N>, tal: AttrL<N>, rt: RawTable<Val>, tt: TType<N, FType>)
		requires welltypedRawtable(tt, rt)
		requires matchingAttrL(tt, tal)
		requires projectTypeAttrL(al, tt).some?
		requires projectCols(al, tal, rt).some?
		ensures welltypedRawtable(projectTypeAttrL(al, tt).get, projectCols(al, tal, rt).get)
	{
		if |al| == 0
		{ welltypedEmptyProjection<N, FType, Val>(rt); }
		else
		{
			findColPreservesWelltypedRaw(al[0], tal, rt, tt);
			findColPreservesRowCount(al[0], tal, rt);
			projectColsPreservesRowCount(tt, al[1..], tal, rt);
			attachColToFrontRawPreservesWelltypedRaw([(al[0],findColType(al[0], tt).get)], projectTypeAttrL(al[1..], tt).get,
			 findCol(al[0], tal, rt).get, projectCols(al[1..], tal, rt).get);
		}
	}
		

  lemma projectTableWelltypedWithSelectType<N, FType, Val>(sel: Select<N>, t: Table<N, Val>, tt: TType<N, FType>)
    requires welltypedtable(tt, t)
    requires projectType(sel, tt).some?
    requires projectTable(sel, t).some? // projectTableProgress proves that this gives a result
    ensures welltypedtable(projectType(sel, tt).get, projectTable(sel, t).get)
  {
    match sel 
    {
      case all =>
      case list(al) => {
        projectTypeAttrLMatchesAttrL(al, tt);
				projectColsWelltypedWithSelectType(al, t.getAL, t.getRaw, tt);
      }
    }
  }

  lemma preservation<N, FType, Val>(ttc: TTContext<N, FType>, ts: TStore<N, Val>, q: Query<N, Val>, q': Query<N, Val>, tt: TType<N, FType>)
    requires StoreContextConsistent(ts, ttc)
    requires typable(ttc, q, tt)
    requires reduce(q, ts) == some(q')
    ensures typable(ttc, q', tt)
  {
    match q
    {
      case tvalue(t) =>
      case selectFromWhere(sel, refs, p) =>
        if |refs| == 1
        {
          successfulLookup(ttc, ts, refs[0]);
					welltypedLookup(ttc, ts, refs[0]);
					var t := lookupEnv(refs[0], ts).get;
          var tt' := lookupEnv(refs[0], ttc).get;
					filterPreservesType(tt', t, p);
          projectTableWelltypedWithSelectType(sel, filterTable(t, p), tt');
        }
        else {}
      case union(q1, q2) =>
        if q1.tvalue?
        {
          if q2.tvalue?
          {rawUnionPreservesWellTypedRaw(q1.getTable.getRaw, q2.getTable.getRaw, tt);}
          else
          {}
        }
        else
        {}
      case intersection(q1, q2) =>
        if q1.tvalue?
        {
          if q2.tvalue?
          {rawIntersectionPreservesWellTypedRaw(q1.getTable.getRaw, q2.getTable.getRaw, tt);}
          else
          {}
        }
        else
        {}
      case difference(q1, q2) =>
        if q1.tvalue?
        {
          if q2.tvalue?
          {rawDifferencePreservesWellTypedRaw(q1.getTable.getRaw, q2.getTable.getRaw, tt);}
          else
          {}
        }
        else
        {}     
    }
  }


}
