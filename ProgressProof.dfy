include "SQLTypeSystem.dfy"
include "SQLSemantics.dfy"
include "SoundnessAuxDefs.dfy"

module Progress
{
	import opened Environments
	import opened Tables
	import opened TableTypes
	import opened Opt
	import opened Queries
	import opened TypeSystem
	import opened QuerySemantics
	import opened StoreContextConsistency

	lemma successfulLookup<N, FType, Val>(ttc: TTContext<N, FType>, ts: TStore<N, Val>, ref: N)
		requires StoreContextConsistent(ts, ttc)
		requires lookupEnv(ref, ttc).some?
		ensures lookupEnv(ref, ts).some?
	{
	}

	lemma welltypedLookup<N, FType, Val>(ttc: TTContext<N, FType>, ts: TStore<N, Val>, ref: N)
		requires StoreContextConsistent(ts, ttc)
		requires lookupEnv(ref, ttc).some?
		requires lookupEnv(ref, ts).some?
		ensures welltypedtable(lookupEnv(ref, ttc).get, lookupEnv(ref, ts).get)
	{
	}

  lemma dropFirstColRawPreservesWelltypedRaw<N, FType, Val>(tt: TType<N, FType>, rt: RawTable<Val>)
    requires |tt| > 0
    requires welltypedRawtable(tt, rt)
    ensures welltypedRawtable(tt[1..], dropFirstColRaw(rt))
  {}

	lemma findColTypeImpliesfindCol<N, FType, Val>(n: N, tt: TType<N, FType>, al: AttrL<N>, rt: RawTable<Val>)
		requires welltypedRawtable(tt, rt)
		requires matchingAttrL(tt, al)
		requires findColType(n, tt).some?
		ensures findCol(n, al, rt).some?
	{
		if |al| == 0
		{}
		else
		{
			if n == al[0]
			{}
			else
			{
        dropFirstColRawPreservesWelltypedRaw(tt, rt);
				findColTypeImpliesfindCol(n, tt[1..], al[1..], dropFirstColRaw(rt));
			}
		}
	}

	lemma projectColsProgress<N, FType, Val>(alt: AttrL<N>, tt: TType<N, FType>, rt: RawTable<Val>, al: AttrL<N>)
		requires welltypedRawtable(tt, rt)
		requires matchingAttrL(tt, alt)
		requires projectTypeAttrL(al, tt).some?
		ensures projectCols(al, alt, rt).some?
	{
		if |al| == 0
		{}
		else
		{
                    //projectTypeImpliesfindCol(tt, alt, rt, al[0], al[1..]);
                    findColTypeImpliesfindCol(al[0], tt, alt, rt);
                    //var col := findCol(al[0], alt, rt).get;
                    //projectAttrLRest(al[0], al[1..], tt);
                }
	}
	
	lemma projectTableProgress<N, FType, Val>(s: Select<N>, tt: TType<N, FType>, t: Table<N, Val>)
		requires welltypedtable(tt, t)
		requires projectType(s, tt).some?
		ensures projectTable(s, t).some?
	{
		match s
		{
			case all =>
			case list(al) => projectColsProgress(t.getAL, tt, t.getRaw, al);
		}
	}

  lemma filterRowsPreservesTable<N, FType, Val>(tt: TType<N, FType>, rt: RawTable<Val>, al: AttrL<N>, p: Pred<N, Val>)
    requires welltypedRawtable(tt, rt)
    ensures welltypedRawtable(tt, filterRows(rt, al, p))
  {}

	lemma filterPreservesType<N, FType, Val>(tt: TType<N, FType>, t: Table<N, Val>, p: Pred<N, Val>)
		requires welltypedtable(tt, t)
		ensures welltypedtable(tt, filterTable(t, p))
	{
    filterRowsPreservesTable(tt, t.getRaw, t.getAL, p);
	}

	lemma progress<N, FType, Val>(ttc: TTContext<N, FType>, ts: TStore<N, Val>, q: Query<N, Val>)
		requires StoreContextConsistent(ts, ttc)
		requires exists tt :: typable(ttc, q, tt)
		ensures isValue(q) || reduce(q, ts).some?
	{
		match q
		{
			case tvalue(t) =>
			case selectFromWhere(sel, refs, p) =>
				if |refs| == 1
				{
				  successfulLookup(ttc, ts, refs[0]);
					welltypedLookup(ttc, ts, refs[0]);
					var t := lookupEnv(refs[0], ts).get;
					var tt := lookupEnv(refs[0], ttc).get;
					filterPreservesType(tt, t, p);
					projectTableProgress(sel, tt, filterTable(t, p));
				}
				else {}
			case union(q1, q2) =>
			case intersection(q1, q2) =>
			case difference(q1, q2) => 
		}
	}
	
}
