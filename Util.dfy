// datatypes and functions generally useful

// support for option datatype
module Opt {
	datatype Option<A> = none | some(get: A)
}

//generic environment datatype, e.g. for table stores and table type contexts
module Environments {
	import opened Opt
	//how to better modularize this?
  
  datatype Env<N, A> = empty | bind(name: N, elem: A, rest: Env<N, A>)

	function lookupEnv<N, A>(name: N, env: Env<N, A>): Option<A>
	{
		match env
		{
			case empty => none
			case bind(m, a, e) =>
				if name == m
				then some(a)
				else lookupEnv(name, e) 
		}
	}	
}

