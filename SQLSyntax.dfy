include "Tables.dfy"
module Queries
{
	import opened Tables

	datatype Select<N> = all | list(getList: AttrL<N>)

	type TRef<N> = seq<N>

	datatype Exp<N, Val> = constant(Val) | lookup(N)

	datatype Pred<N, Val> = ptrue
		| and(Pred, Pred)
		| not(Pred)
		| eq(Exp<N, Val>, Exp<N, Val>)
		| gt(Exp<N, Val>, Exp<N, Val>)
		| lt(Exp<N, Val>, Exp<N, Val>)
		
	datatype Query<N, Val> = tvalue(getTable: Table<N, Val>)
		| selectFromWhere(Select<N>, TRef<N>, Pred<N, Val>)
		| union(Query<N, Val>, Query<N, Val>)
		| intersection(Query<N, Val>, Query<N, Val>)
		| difference(Query<N, Val>, Query<N, Val>)

	predicate isValue<N, Val>(q: Query<N, Val>)
	{
		q.tvalue?
	}	
}
