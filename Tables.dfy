include "Util.dfy"

// untyped tables and basic functions to manipulate them
module Tables {
	import opened Opt
	// attributes and field values and types
	type AttrL<N> = seq<N>

	// how to specify default cases in Dafny?
	predicate greaterThan<Val>(v1: Val, v2: Val)

	predicate lessThan<Val>(v1: Val, v2: Val)


	// "raw" table without header	
	type Row<Val> = seq<Val>
	type RawTable<Val> = seq<Row<Val>>
		
	// full table with header (attribute list) 
	datatype Table<N, Val> = table(getAL: AttrL<N>, getRaw: RawTable<Val>)

			// rowIn is just the infix "in" operator on sequences in Dafny ;)

	//projects a raw table to its first column
  //returns a raw table with exactly one column or tempty
	//TODO: is it a good idea to specify this as recursive function...?
	function projectFirstRaw<Val>(rt: RawTable<Val>): RawTable<Val>
	{
		if |rt| == 0
			then []
		else if rt[0] == []
			then [[]] + projectFirstRaw(rt[1..])
			else [[rt[0][0]]] + projectFirstRaw(rt[1..])
	}

	//drops the first column of a raw table
	//returns a raw table with one column less than before or tempty
	function dropFirstColRaw<Val>(rt: RawTable<Val>): RawTable<Val>
	{
		if |rt| == 0
			then []
		else if rt[0] == []
			then [[]] + dropFirstColRaw(rt[1..])
			else [rt[0][1..]] + dropFirstColRaw(rt[1..])
	}

	//attaches a raw table with one column to the front of another raw table
	//returns a raw table with one column more
	//blindly assumes that both tables have the same row count!
	//fails if input tables do not have the desired format
	//let-expressions would be nice for this specification...
	function attachColToFrontRaw<Val>(rt1: RawTable<Val>, rt2: RawTable<Val>): RawTable<Val>
	{
		if |rt1| == 0 && |rt2| == 0
			then []
		else if |rt1| > 0 && |rt1[0]| == 1 && |rt2| > 0
			then [[rt1[0][0]] + rt2[0]] + attachColToFrontRaw(rt1[1..], rt2[1..])
		else [[]] //ERROR CASE! idea: return a term which will cause the resulting table to be not welltyped...
			//should we model the explicitly (with option type)...? could this cause a soundness error later?
	}

	//blind row union - does not care about table types!
	//definition: union removes duplicate rows
	//(but only between the two tables, not within a table!)
	//preserves row order of the two original raw tables
	function rawUnion<Val>(rt1: RawTable<Val>, rt2: RawTable<Val>): RawTable<Val>
	{
		if |rt1| == 0
			then rt2
		else if |rt2| == 0
			then rt1
		else if rt1[0] in rt2
			then rawUnion(rt1[1..], rt2)
		else [rt1[0]] + rawUnion(rt1[1..], rt2)
	}

	//preserves order of rows in first argument
	//also ignores table types completely
	function rawIntersection<Val>(rt1: RawTable<Val>, rt2: RawTable<Val>): RawTable<Val>
	{
		if |rt1| == 0
			then []
		else if |rt2| == 0
			then []
		else if rt1[0] in rt2
			then [rt1[0]] + rawIntersection(rt1[1..], rt2)
		else rawIntersection(rt1[1..], rt2)
	}

	function rawDifference<Val>(rt1: RawTable<Val>, rt2: RawTable<Val>): RawTable<Val>
	{
		if |rt1| == 0
			then []
		else if |rt2| == 0
			then rt1
		else if rt1[0] in rt2
			then rawDifference(rt1[1..], rt2)
		else [rt1[0]] + rawDifference(rt1[1..], rt2)
	}

	//project one column out of a raw table, if the column can be found (fails otherwise)
	//TODO: explicitly model failure here or just return an empty table?
	function findCol<N, Val>(a: N, al: AttrL<N>, rt: RawTable<Val>): Option<RawTable<Val>>
	{
		if |al| == 0
			then none
		else if a == al[0]
			then some(projectFirstRaw(rt))
		else findCol(a, al[1..], dropFirstColRaw(rt))
	}


	// project an empty column with as many rows as the given raw table
	function projectEmptyCol<Val>(rt: RawTable<Val>): RawTable<Val>
	{
		if |rt| == 0
		then []
		else [[]] + projectEmptyCol(rt[1..])
	}

	function projectCols<N, Val>(projectOn: AttrL<N>, tableheader: AttrL<N>, rt: RawTable<Val>): Option<RawTable<Val>>
	{
		if |projectOn| == 0
			then some(projectEmptyCol(rt))
		else if findCol(projectOn[0], tableheader, rt).some? && projectCols(projectOn[1..], tableheader, rt).some?
			then some(attachColToFrontRaw(findCol(projectOn[0], tableheader, rt).get, projectCols(projectOn[1..], tableheader, rt).get))
			else none
	}
}

// typed tables and definition of well-typedness of a table
module TableTypes {
	import opened Tables

	//assign types to field values (currently static)
	function fieldType<Val, FType>(v: Val): FType
	
	//typed table schemas
	type TType<N, FType> = seq<(N, FType)>

	//welltypedness of tables
	predicate matchingAttrL<N, FType>(tt: TType<N, FType>, al: AttrL<N>)
	{
		|tt| == |al| && forall i :: 0 <= i < |tt| ==> tt[i].0 == al[i] 
	}

	predicate welltypedRow<N, FType, Val>(tt: TType<N, FType>, r: Row<Val>)
	{
		|tt| == |r| && forall i :: 0 <= i < |tt| ==> fieldType(r[i]) == tt[i].1
	}
	
	predicate welltypedRawtable<N, FType, Val>(tt: TType<N, FType>, t: RawTable<Val>)
	{
		forall i :: 0 <= i < |t| ==> welltypedRow(tt, t[i])
	}

	predicate welltypedtable<N, FType, Val>(tt: TType<N, FType>, t: Table<N, Val>)
	{
	  matchingAttrL(tt, t.getAL) && welltypedRawtable(tt, t.getRaw)	
	}
}

